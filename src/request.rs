use super::MemoryProperties;

/// Memory request.
#[derive(Clone, Copy, Debug)]
pub struct MemoryRequest {
    /// Size.
    pub size: u32,
    /// Alignment. Should be a power of 2.
    pub alignment: u32,
    /// Memory type mask. Set to !0 to allow all types.
    pub type_mask: u32,
    /// Memory usage.
    pub usage: MemoryUsage,
    /// Memory contents.
    pub contents: MemoryContents,
    /// Force dedicated allocation. Works only with DynamicAllocator.
    pub force_dedicated: bool,
}

/// Memory contents (linear or optimal).
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum MemoryContents {
    /// Unknown.
    Unknown,
    /// Buffer.
    Buffer,
    /// Image with unknown tiling.
    ImageUnknown,
    /// Image with linear tiling.
    ImageLinear,
    /// Image with optimal tiling.
    ImageOptimal,
}

impl MemoryContents {
    pub fn conflict(mut self, mut other: MemoryContents) -> bool {
        if self as u8 > other as u8 {
            std::mem::swap(&mut self, &mut other);
        }

        match self {
            MemoryContents::Unknown => true,
            MemoryContents::Buffer => {
                other == MemoryContents::ImageUnknown || other == MemoryContents::ImageOptimal
            }
            MemoryContents::ImageUnknown => {
                other == MemoryContents::ImageUnknown
                    || other == MemoryContents::ImageLinear
                    || other == MemoryContents::ImageOptimal
            }
            MemoryContents::ImageLinear => other == MemoryContents::ImageOptimal,
            MemoryContents::ImageOptimal => false,
        }
    }
}

/// Memory usage, which specifies memory type.
#[derive(Clone, Copy, Debug)]
pub enum MemoryUsage {
    /// Used for static data and render attachments.
    GpuOnly,
    /// Used for lazily allocated transient render attachments.
    GpuLazilyAllocated,
    /// Used for staging buffers.
    CpuOnly,
    /// Used for dynamic data uploaded every frame.
    CpuToGpu,
    /// Used for readback data.
    GpuToCpu,
}

impl MemoryUsage {
    pub(crate) fn supports(self, props: MemoryProperties) -> bool {
        use MemoryProperties as P;
        use MemoryUsage::*;

        match self {
            GpuOnly => true,
            GpuLazilyAllocated => props.contains(P::LAZILY_ALLOCATED),
            CpuOnly => props.contains(P::CPU_VISIBLE | P::COHERENT),
            CpuToGpu | GpuToCpu => props.contains(P::CPU_VISIBLE),
        }
    }

    pub(crate) fn priority(self, props: MemoryProperties) -> u32 {
        use MemoryProperties as P;
        use MemoryUsage::*;

        match self {
            GpuOnly | GpuLazilyAllocated | CpuToGpu => {
                // prefer device local
                !props.contains(P::DEVICE_LOCAL) as u32
            }
            CpuOnly | GpuToCpu => {
                // prefer cached
                !props.contains(P::CPU_CACHED) as u32
            }
        }
    }
}
