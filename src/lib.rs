//! gfx_hal memory allocator

mod allocator;
mod info;
mod pool;
mod report;
mod request;
mod util;

pub use gfx_hal::memory::Properties as MemoryProperties;

pub use self::allocator::{
    AllocationError, Allocator, DedicatedAllocator, DynamicAllocator, DynamicAllocatorConfig,
    FreeResult, LinearAllocator, LinearAllocatorConfig, MemoryBlock, TreeAllocator,
    TreeAllocatorConfig,
};
pub use self::info::{MemoryInfo, MemoryTypeId, MemoryTypes};
pub use self::pool::{MemoryPool, ReadMapping, WriteMapping};
pub use self::report::ReportBuilder;
pub use self::request::{MemoryContents, MemoryRequest, MemoryUsage};
