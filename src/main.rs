use std::sync::Arc;

use allocator::{
    Allocator, DedicatedAllocator, MemoryContents, MemoryInfo, MemoryPool, MemoryRequest,
    MemoryTypes, MemoryUsage, TreeAllocator,
};
use anyhow::Result;
use gfx_backend_vulkan::Backend as VkBackend;
use gfx_hal::prelude::PhysicalDevice;
use gfx_hal::{Backend, Features, Instance};
use rand::prelude::*;

fn main() -> Result<()> {
    env_logger::init();

    let instance = gfx_backend_vulkan::Instance::create("app", 1)?;
    let adapter = {
        let mut adapters = instance.enumerate_adapters();
        adapters.remove(0)
    };

    let gpu = unsafe {
        adapter
            .physical_device
            .open(&[(&adapter.queue_families[0], &[1.0])], Features::empty())?
    };
    let device = gpu.device;

    let limits = adapter.physical_device.properties().limits;
    let properties = adapter.physical_device.memory_properties();

    let memory_types = Arc::new(MemoryTypes::new(&properties.memory_types));

    let memory_info = Arc::new(MemoryInfo {
        non_coherent_atom_size: limits.non_coherent_atom_size as _,
        buffer_image_granularity: limits.buffer_image_granularity as _,
    });

    let mut tree_memory_pool = MemoryPool::<VkBackend, TreeAllocator<VkBackend>>::new(
        memory_types.clone(),
        memory_info.clone(),
    );

    let mut dedic_memory_pool =
        MemoryPool::<VkBackend, DedicatedAllocator>::new(memory_types, memory_info);

    for p in 0..6 {
        let amt = 10usize.pow(p);
        let tree = bench(&device, &mut tree_memory_pool, amt)?;
        let dedic = bench(&device, &mut dedic_memory_pool, amt)?;
        println!("{},{},{}", amt, tree, dedic);
    }

    unsafe {
        tree_memory_pool.destroy(&device);
        dedic_memory_pool.destroy(&device);
    }

    Ok(())
}

fn bench<B: Backend, A: Allocator<B>>(
    device: &B::Device,
    pool: &mut MemoryPool<B, A>,
    amt: usize,
) -> Result<f64> {
    let t = std::time::Instant::now();
    for _ in 0..10 {
        let mut blocks = Vec::new();
        let mut rng = rand::thread_rng();

        for _ in 0..amt {
            let block = pool.alloc(
                &device,
                &MemoryRequest {
                    size: rng.gen_range(1024..=10240),
                    alignment: 32,
                    type_mask: !0,
                    usage: MemoryUsage::CpuOnly,
                    contents: MemoryContents::Buffer,
                    force_dedicated: false,
                },
            )?;

            blocks.push(block);
        }

        blocks.shuffle(&mut rng);

        for block in blocks.drain(..amt / 2) {
            unsafe {
                pool.free(&device, block);
            }
        }

        for _ in 0..amt / 2 {
            let block = pool
                .alloc(
                    &device,
                    &MemoryRequest {
                        size: rng.gen_range(1024..=10240),
                        alignment: 32,
                        type_mask: !0,
                        usage: MemoryUsage::CpuOnly,
                        contents: MemoryContents::Buffer,
                        force_dedicated: false,
                    },
                )
                .unwrap();

            blocks.push(block);
        }

        for block in blocks.drain(..) {
            unsafe {
                pool.free(&device, block);
            }
        }
    }

    Ok(t.elapsed().as_secs_f64() / 10.0)
}
