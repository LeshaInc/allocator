pub trait NodeId: Copy + Eq {
    const NULL: Self;
}

impl NodeId for u32 {
    const NULL: u32 = u32::MAX;
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Color {
    Red,
    Black,
}

#[derive(Clone, Copy, Debug)]
pub struct RBTreeNode<Id> {
    pub parent: Id,
    pub color: Color,
    pub children: [Id; 2],
}

impl<Id: NodeId> Default for RBTreeNode<Id> {
    fn default() -> Self {
        Self {
            parent: Id::NULL,
            color: Color::Black,
            children: [Id::NULL; 2],
        }
    }
}

pub trait RBTree<Id: NodeId> {
    fn root(&self) -> Id;

    fn root_mut(&mut self) -> &mut Id;

    fn get(&self, id: Id) -> &RBTreeNode<Id>;

    fn get_mut(&mut self, id: Id) -> &mut RBTreeNode<Id>;

    fn order(&self, a: Id, b: Id) -> bool;
}

trait RBTreeUtils<Id: NodeId>: RBTree<Id> {
    fn parent(&self, node: Id) -> Id {
        self.get(node).parent
    }

    fn set_parent(&mut self, node: Id, parent: Id) {
        self.get_mut(node).parent = parent
    }

    fn grandparent(&self, node: Id) -> Id {
        self.parent(self.parent(node))
    }

    fn children(&self, node: Id) -> [Id; 2] {
        self.get(node).children
    }

    fn child(&self, node: Id, side: usize) -> Id {
        self.children(node)[side]
    }

    fn set_child(&mut self, node: Id, side: usize, child: Id) {
        self.get_mut(node).children[side] = child;
    }

    #[inline(always)]
    fn rotate(&mut self, side: usize, x: Id) {
        let y = self.child(x, 1 - side);

        let c = self.child(y, side);
        self.set_child(x, 1 - side, c);
        if c != Id::NULL {
            self.set_parent(c, x);
        }

        let c = self.parent(x);
        self.set_parent(y, c);
        if c == Id::NULL {
            *self.root_mut() = y;
        } else {
            let side = (x == self.child(c, 1)) as usize;
            self.set_child(c, side, y);
        }

        self.set_child(y, side, x);
        self.set_parent(x, y);
    }

    fn color(&self, node: Id) -> Color {
        if node == Id::NULL {
            return Color::Black;
        }

        self.get(node).color
    }

    fn set_color(&mut self, node: Id, color: Color) {
        if node != Id::NULL {
            self.get_mut(node).color = color;
        }
    }

    fn insert_repair(&mut self, mut current: Id) {
        while self.color(self.parent(current)) == Color::Red {
            if self.grandparent(current) == Id::NULL {
                // TODO: investigate
                break;
            }

            let side = (self.parent(current) == self.child(self.grandparent(current), 1)) as usize;
            let uncle = self.child(self.grandparent(current), 1 - side);
            if self.color(uncle) == Color::Red {
                self.set_color(self.parent(current), Color::Black);
                self.set_color(uncle, Color::Black);
                current = self.grandparent(current);
                self.set_color(current, Color::Red);
            } else {
                if current == self.child(self.parent(current), 1 - side) {
                    current = self.parent(current);
                    self.rotate(side, current);
                }

                self.set_color(self.parent(current), Color::Black);
                self.set_color(self.grandparent(current), Color::Red);
                self.rotate(1 - side, self.grandparent(current));
            }
        }
    }

    fn swap(&mut self, a: Id, b: Id) {
        let a_parent = self.parent(a);
        let a_children = self.children(a);
        let b_parent = self.parent(b);
        let b_children = self.children(b);

        if b_parent != a {
            self.set_parent(a, b_parent);
            if b_parent != Id::NULL {
                let idx = (b == self.child(b_parent, 1)) as usize;
                self.set_child(b_parent, idx, a);
            }
        }

        if a_parent != b {
            self.set_parent(b, a_parent);
            if a_parent != Id::NULL {
                let idx = (a == self.child(a_parent, 1)) as usize;
                self.set_child(a_parent, idx, b);
            }
        }

        for i in 0..2 {
            if b_children[i] != a {
                self.set_child(a, i, b_children[i]);
                if b_children[i] != Id::NULL {
                    self.set_parent(b_children[i], a);
                }
            } else {
                self.set_child(a, i, b);
                self.set_parent(b, a);
            }

            if a_children[i] != b {
                self.set_child(b, i, a_children[i]);
                if a_children[i] != Id::NULL {
                    self.set_parent(a_children[i], b);
                }
            } else {
                self.set_child(b, i, a);
                self.set_parent(a, b);
            }
        }

        if self.parent(a) == Id::NULL {
            *self.root_mut() = a;
        } else if self.parent(b) == Id::NULL {
            *self.root_mut() = b;
        }
    }

    fn remove_repair(&mut self, mut current: Id) {
        while current != self.root() {
            let side = (current == self.child(self.parent(current), 1)) as usize;
            let mut sibling = self.child(self.parent(current), 1 - side);

            if self.color(sibling) == Color::Red {
                self.set_color(sibling, Color::Black);
                self.set_color(self.parent(current), Color::Red);
                self.rotate(side, self.parent(current));
                sibling = self.child(self.parent(current), 1 - side);
            }

            if sibling == Id::NULL {
                break;
            }

            if self.color(self.child(sibling, 0)) == Color::Black
                && self.color(self.child(sibling, 1)) == Color::Black
            {
                self.set_color(sibling, Color::Red);
                if self.color(self.parent(current)) == Color::Red {
                    self.set_color(self.parent(current), Color::Black);
                    break;
                } else {
                    current = self.parent(current);
                }
            } else {
                if self.color(self.child(sibling, 1 - side)) == Color::Black {
                    self.set_color(self.child(sibling, side), Color::Black);
                    self.set_color(sibling, Color::Red);
                    self.rotate(1 - side, sibling);
                    sibling = self.child(self.parent(sibling), 1 - side);
                }

                self.set_color(sibling, self.color(self.parent(sibling)));
                self.set_color(self.parent(current), Color::Black);
                self.set_color(self.child(sibling, 1 - side), Color::Black);
                self.rotate(side, self.parent(current));
                break;
            }
        }
    }

    fn find_min_helper(&self, node: Id, filter: &mut impl FnMut(Id) -> bool) -> Id {
        if filter(node) {
            let left = self.child(node, 0);
            if left == Id::NULL {
                return node;
            }

            let left_res = self.find_min_helper(left, filter);
            if left_res == Id::NULL {
                node
            } else {
                left_res
            }
        } else {
            let right = self.child(node, 1);
            if right == Id::NULL {
                return Id::NULL;
            }

            self.find_min_helper(right, filter)
        }
    }
}

impl<Id: NodeId, T: ?Sized + RBTree<Id>> RBTreeUtils<Id> for T {}

pub trait RBTreeOps<Id: NodeId>: RBTree<Id> {
    fn insert(&mut self, node: Id) {
        let mut parent = self.root();
        let mut side = 0;

        let mut current = if parent == Id::NULL {
            parent
        } else {
            side = self.order(node, parent) as usize;
            self.child(parent, side)
        };

        while current != Id::NULL {
            parent = current;
            side = self.order(node, current) as usize;
            current = self.child(current, side);
        }

        current = node;
        self.set_parent(current, parent);
        self.set_child(current, 0, Id::NULL);
        self.set_child(current, 1, Id::NULL);

        if parent == Id::NULL {
            *self.root_mut() = current;
            self.set_color(current, Color::Black);
            return;
        }

        self.set_child(parent, side, current);
        self.set_color(current, Color::Red);

        if self.color(parent) == Color::Red {
            self.insert_repair(current);
        }

        self.set_color(self.root(), Color::Black);
    }

    fn remove(&mut self, node: Id) {
        if self.child(node, 0) != Id::NULL && self.child(node, 1) != Id::NULL {
            let succ = self.successor(node);
            self.swap(node, succ);
        } else if self.root() == node
            && self.child(node, 0) == Id::NULL
            && self.child(node, 1) == Id::NULL
        {
            *self.root_mut() = Id::NULL;
            return;
        }

        let idx = (self.child(node, 0) == Id::NULL) as usize;
        let child = self.child(node, idx);

        if self.color(node) == Color::Black {
            if self.color(child) == Color::Red {
                self.set_color(child, Color::Black);
            } else if node != self.root() {
                self.remove_repair(node);
            }
        }

        let p = self.parent(node);
        if child != Id::NULL {
            self.set_parent(child, p);
            if p == Id::NULL {
                *self.root_mut() = child;
            }
        }

        if self.parent(node) != Id::NULL {
            let idx = (node == self.child(self.parent(node), 1)) as usize;
            self.set_child(self.parent(node), idx, child);
        }

        self.set_parent(node, Id::NULL);
        self.set_child(node, 0, Id::NULL);
        self.set_child(node, 1, Id::NULL);
    }

    fn successor(&self, mut node: Id) -> Id {
        if self.child(node, 1) != Id::NULL {
            let mut node = self.child(node, 1);
            while self.child(node, 0) != Id::NULL {
                node = self.child(node, 0);
            }
            node
        } else {
            let mut succ = self.parent(node);
            while succ != Id::NULL && node == self.child(succ, 1) {
                node = succ;
                succ = self.parent(succ);
            }
            succ
        }
    }

    fn find_min(&self, mut filter: impl FnMut(Id) -> bool) -> Id {
        if self.root() == Id::NULL {
            return Id::NULL;
        }

        self.find_min_helper(self.root(), &mut filter)
    }
}

impl<Id: NodeId, T: RBTree<Id>> RBTreeOps<Id> for T {}
