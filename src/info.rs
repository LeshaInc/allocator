use gfx_hal::adapter::MemoryType;

use super::{MemoryProperties, MemoryUsage};

/// Memory info, returned by device
#[derive(Debug)]
pub struct MemoryInfo {
    /// Non coherent atom size. Ignored for not host visible or coherent memory types.
    pub non_coherent_atom_size: u32,
    /// Buffer image granularity.
    pub buffer_image_granularity: u32,
}

/// Memory types, returned by device.
#[derive(Debug)]
pub struct MemoryTypes {
    usages: [[MemoryTypeEntry; 32]; 5],
    properties: [MemoryProperties; 32],
    num_types: u8,
}

/// ID of a memory type.
#[derive(Clone, Copy, Debug)]
pub struct MemoryTypeId(pub(crate) u8);

#[derive(Clone, Copy, Debug)]
struct MemoryTypeEntry {
    id: MemoryTypeId,
    properties: MemoryProperties,
}

impl MemoryTypes {
    /// Convert device memory types to `MemoryTypes` instance.
    ///
    /// Number of types should not exceed 32.
    pub fn new(types: &[MemoryType]) -> MemoryTypes {
        assert!(types.len() <= 32);

        let mut out = [[MemoryTypeEntry {
            id: MemoryTypeId(255),
            properties: MemoryProperties::empty(),
        }; 32]; 5];

        let usages = [
            MemoryUsage::GpuOnly,
            MemoryUsage::GpuLazilyAllocated,
            MemoryUsage::CpuOnly,
            MemoryUsage::CpuToGpu,
            MemoryUsage::GpuToCpu,
        ];

        for (usage_idx, &usage) in usages.iter().enumerate() {
            let mut list_len = 0;

            for (ty_idx, ty) in types.iter().enumerate() {
                if usage.supports(ty.properties) {
                    out[usage_idx][list_len] = MemoryTypeEntry {
                        id: MemoryTypeId(ty_idx as u8),
                        properties: ty.properties,
                    };
                    list_len += 1;
                }
            }

            out[usage_idx][..list_len]
                .sort_by_key(|entry| usage.priority(types[entry.id.0 as usize].properties));
        }

        let mut properties = [MemoryProperties::empty(); 32];
        for (i, memory_type) in types.iter().enumerate() {
            properties[i] = memory_type.properties;
        }

        MemoryTypes {
            usages: out,
            num_types: types.len() as _,
            properties,
        }
    }

    pub(crate) fn num_types(&self) -> u8 {
        self.num_types
    }

    pub(crate) fn get(
        &self,
        usage: MemoryUsage,
    ) -> impl Iterator<Item = (MemoryTypeId, MemoryProperties)> + '_ {
        self.usages[usage as usize]
            .iter()
            .copied()
            .take_while(|&entry| entry.id.0 != 255)
            .map(|entry| (entry.id as _, entry.properties))
    }

    pub(crate) fn properties(&self, memory_type: MemoryTypeId) -> MemoryProperties {
        self.properties[memory_type.0 as usize]
    }
}
