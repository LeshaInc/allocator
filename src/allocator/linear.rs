use std::sync::Arc;

use gfx_hal::device::Device;
use gfx_hal::Backend;

use super::{
    AllocationError, Allocator, FreeResult, MemoryBlock, MemoryInfo, MemoryProperties,
    MemoryRequest, MemoryTypeId,
};
use crate::util::align_up;
use crate::MemoryContents;

/// Linear allocation config.
#[derive(Clone, Copy, Debug)]
pub struct LinearAllocatorConfig {
    /// Allocate chunks multiple of X.
    pub chunk_multiple: u32,
    /// Minimum chunk size.
    pub chunk_min_size: u32,
    /// Maximum bytes left in chunk to consider it closed.
    pub chunk_max_wasted: u32,
    /// Keep X bytes of chunks even when freed.
    pub keep_bytes: u32,
}

impl Default for LinearAllocatorConfig {
    fn default() -> Self {
        Self {
            // TODO: tweak
            chunk_multiple: 1024 * 1024,
            chunk_min_size: 1024 * 1024,
            chunk_max_wasted: 512,
            keep_bytes: 16 * 1024 * 1024,
        }
    }
}

/// Linear (bump) allocator.
pub struct LinearAllocator<B: Backend> {
    open_chunks: Vec<LinearChunk<B>>,
    full_chunks: Vec<LinearChunk<B>>,
    total_size: u64,
    config: Arc<LinearAllocatorConfig>,
}

struct LinearChunk<B: Backend> {
    memory: Box<B::Memory>,
    size: u32,
    used: u32,
    last_contents: Option<MemoryContents>,
}

impl<B: Backend> LinearAllocator<B> {
    /// Create a new linear allocator with specified config.
    pub fn new(config: Arc<LinearAllocatorConfig>) -> LinearAllocator<B> {
        LinearAllocator {
            open_chunks: Vec::new(),
            full_chunks: Vec::new(),
            total_size: 0,
            config,
        }
    }

    /// Free all allocated blocks at once. Blocks must be not in use.
    pub unsafe fn free_all(&mut self, device: &B::Device) {
        self.open_chunks.append(&mut self.full_chunks);
        self.open_chunks
            .sort_unstable_by(|l, r| r.size.cmp(&l.size));

        let mut sum = 0;
        let num_kept = self
            .open_chunks
            .iter()
            .take_while(|chunk| {
                sum += chunk.size;
                sum < self.config.keep_bytes
            })
            .count();

        for chunk in self.open_chunks.drain(num_kept..) {
            device.free_memory(*chunk.memory);
        }

        for chunk in &mut self.open_chunks {
            chunk.used = 0;
            chunk.last_contents = None;
        }
    }
}

impl<B: Backend> Default for LinearAllocator<B> {
    fn default() -> Self {
        Self::new(Default::default())
    }
}

impl<B: Backend> Allocator<B> for LinearAllocator<B> {
    fn alloc(
        &mut self,
        device: &B::Device,
        memory_info: &MemoryInfo,
        memory_type: MemoryTypeId,
        properties: MemoryProperties,
        req: &MemoryRequest,
    ) -> Result<MemoryBlock<B>, AllocationError> {
        let alignment = if properties.contains(MemoryProperties::CPU_VISIBLE)
            && !properties.contains(MemoryProperties::COHERENT)
        {
            align_up(req.alignment, memory_info.non_coherent_atom_size)
        } else {
            req.alignment
        };

        for (chunk_idx, chunk) in self.open_chunks.iter_mut().enumerate() {
            let additional_alignment = match chunk.last_contents {
                Some(v) if v.conflict(req.contents) => memory_info.buffer_image_granularity,
                _ => 0,
            };
            let alignment = alignment.max(additional_alignment);
            let offset = align_up(chunk.used, alignment);
            if offset + req.size > chunk.size {
                continue;
            }

            let block = MemoryBlock {
                memory: &*chunk.memory as *const _,
                memory_type,
                allocator: 0,
                metadata: 0,
                offset,
                size: req.size,
            };

            chunk.used = offset + req.size;
            chunk.last_contents = Some(req.contents);

            if chunk.size - chunk.used <= self.config.chunk_max_wasted {
                let chunk = self.open_chunks.swap_remove(chunk_idx);
                self.full_chunks.push(chunk);
            }

            return Ok(block);
        }

        let new_chunk_size =
            align_up(req.size, self.config.chunk_multiple).max(self.config.chunk_min_size);
        self.total_size += new_chunk_size as u64;

        let memory = unsafe {
            device
                .allocate_memory(
                    gfx_hal::MemoryTypeId(memory_type.0 as usize),
                    new_chunk_size as u64,
                )
                .map(Box::new)?
        };

        let memory_ptr = &*memory as *const _;

        let new_chunk = LinearChunk {
            memory,
            size: new_chunk_size,
            used: req.size,
            last_contents: Some(req.contents),
        };

        if new_chunk.used < new_chunk.size {
            self.open_chunks.push(new_chunk);
        } else {
            self.full_chunks.push(new_chunk);
        }

        Ok(MemoryBlock {
            memory: memory_ptr,
            memory_type,
            allocator: 0,
            metadata: 0,
            offset: 0,
            size: req.size,
        })
    }

    unsafe fn free(&mut self, _device: &B::Device, _block: MemoryBlock<B>) -> FreeResult {
        FreeResult::ChunkInUse
    }

    unsafe fn destroy(mut self, device: &B::Device) {
        for chunk in self.open_chunks.drain(..) {
            device.free_memory(*chunk.memory);
        }

        for chunk in self.full_chunks.drain(..) {
            device.free_memory(*chunk.memory);
        }
    }
}
