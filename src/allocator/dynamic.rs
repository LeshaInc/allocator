use std::sync::Arc;

use gfx_hal::Backend;

use super::{
    AllocationError, Allocator, DedicatedAllocator, FreeResult, MemoryBlock, MemoryInfo,
    MemoryProperties, MemoryRequest, MemoryTypeId, TreeAllocator, TreeAllocatorConfig,
};
use crate::ReportBuilder;

/// Dynamic allocator config.
#[derive(Clone, Copy, Debug)]
pub struct DynamicAllocatorConfig {
    /// Allocate dedicated blocks when size >= X.
    pub dedicated_threshold: u32,
}

impl Default for DynamicAllocatorConfig {
    fn default() -> Self {
        Self {
            // TODO: tweak
            dedicated_threshold: 32 * 1024 * 1024,
        }
    }
}

/// Dynamic allocator. Picks between tree and linear depending on size.
pub struct DynamicAllocator<B: Backend> {
    config: Arc<DynamicAllocatorConfig>,
    tree: TreeAllocator<B>,
    dedicated: DedicatedAllocator,
}

impl<B: Backend> Default for DynamicAllocator<B> {
    fn default() -> Self {
        Self::new(Default::default(), Default::default())
    }
}

impl<B: Backend> DynamicAllocator<B> {
    /// Create a new dynamic allocator.
    pub fn new(
        config: Arc<DynamicAllocatorConfig>,
        tree_config: Arc<TreeAllocatorConfig>,
    ) -> DynamicAllocator<B> {
        Self {
            config,
            tree: TreeAllocator::new(tree_config),
            dedicated: DedicatedAllocator::new(),
        }
    }
}

impl<B: Backend> Allocator<B> for DynamicAllocator<B> {
    fn alloc(
        &mut self,
        device: &B::Device,
        memory_info: &MemoryInfo,
        memory_type: MemoryTypeId,
        properties: MemoryProperties,
        req: &MemoryRequest,
    ) -> Result<MemoryBlock<B>, AllocationError> {
        if req.size > self.config.dedicated_threshold || req.force_dedicated {
            let mut block =
                self.dedicated
                    .alloc(device, memory_info, memory_type, properties, req)?;
            block.allocator = 0;
            Ok(block)
        } else {
            let mut block = self
                .tree
                .alloc(device, memory_info, memory_type, properties, req)?;
            block.allocator = 1;
            Ok(block)
        }
    }

    unsafe fn free(&mut self, device: &B::Device, block: MemoryBlock<B>) -> FreeResult {
        if block.allocator == 0 {
            self.dedicated.free(device, block)
        } else {
            self.tree.free(device, block)
        }
    }

    unsafe fn destroy(self, device: &B::Device) {
        self.tree.destroy(device);
        <DedicatedAllocator as Allocator<B>>::destroy(self.dedicated, device);
    }

    fn report(&self, builder: &mut ReportBuilder) {
        self.tree.report(builder);
        <DedicatedAllocator as Allocator<B>>::report(&self.dedicated, builder);
    }
}
