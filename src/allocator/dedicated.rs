use gfx_hal::device::Device;
use gfx_hal::Backend;

use super::{
    AllocationError, Allocator, FreeResult, MemoryBlock, MemoryInfo, MemoryProperties,
    MemoryRequest, MemoryTypeId,
};

/// Dedicated allocator. Every block gets its own memory chunk.
#[derive(Clone, Copy, Debug, Default)]
pub struct DedicatedAllocator;

impl DedicatedAllocator {
    /// Create a new dedicated allocator.
    pub fn new() -> DedicatedAllocator {
        DedicatedAllocator
    }
}

impl<B: Backend> Allocator<B> for DedicatedAllocator {
    fn alloc(
        &mut self,
        device: &B::Device,
        _memory_info: &MemoryInfo,
        memory_type: MemoryTypeId,
        _properties: MemoryProperties,
        req: &MemoryRequest,
    ) -> Result<MemoryBlock<B>, AllocationError> {
        let memory = unsafe {
            device.allocate_memory(
                gfx_hal::MemoryTypeId(memory_type.0 as usize),
                req.size as u64,
            )?
        };

        let memory_ptr = Box::into_raw(Box::new(memory));

        Ok(MemoryBlock {
            memory: memory_ptr as *const _,
            memory_type,
            allocator: 0,
            metadata: 0,
            offset: 0,
            size: req.size as u32,
        })
    }

    unsafe fn free(&mut self, device: &B::Device, block: MemoryBlock<B>) -> FreeResult {
        let boxed = Box::from_raw(block.memory as *mut _);
        device.free_memory(*boxed);
        FreeResult::ChunkFreed
    }

    unsafe fn destroy(self, _device: &B::Device) {}
}
