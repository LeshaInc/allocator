use std::sync::Arc;

use ahash::AHashSet;
use gfx_hal::device::Device;
use gfx_hal::Backend;
use slab::Slab;

use super::{
    AllocationError, Allocator, FreeResult, MemoryBlock, MemoryInfo, MemoryProperties,
    MemoryRequest, MemoryTypeId,
};
use crate::util::align_up;
use crate::util::rbtree::{RBTree, RBTreeNode, RBTreeOps};
use crate::{MemoryContents, ReportBuilder};

/// Tree allocator config.
#[derive(Clone, Copy, Debug)]
pub struct TreeAllocatorConfig {
    /// Allocate chunks multiple of X.
    pub chunk_multiple: u32,
    /// Minimum chunk size.
    pub chunk_min_size: u32,
}

impl Default for TreeAllocatorConfig {
    fn default() -> Self {
        Self {
            // TODO: tweak
            chunk_multiple: 1024 * 1024,
            chunk_min_size: 100 * 1024 * 1024,
        }
    }
}

/// Tree allocator. Stores free blocks in a red-black tree, and adjacent blocks in a doubly-linked
/// list.
#[derive(Debug)]
pub struct TreeAllocator<B: Backend> {
    config: Arc<TreeAllocatorConfig>,
    blocks: Slab<TreeAllocatorBlock<B>>,
    tree_root: u32,
}

#[derive(Debug)]
struct TreeAllocatorBlock<B: Backend> {
    memory: *const B::Memory,
    offset: u32,
    size: u32,
    tree_node: RBTreeNode<u32>,
    next_block: u32,
    prev_block: u32,
    is_free: bool,
    contents: Option<MemoryContents>,
}

impl<B: Backend> Default for TreeAllocator<B> {
    fn default() -> Self {
        Self::new(Default::default())
    }
}

impl<B: Backend> TreeAllocator<B> {
    /// Create a new tree allocator with specified config.
    pub fn new(config: Arc<TreeAllocatorConfig>) -> Self {
        Self {
            config,
            blocks: Slab::new(),
            tree_root: u32::MAX,
        }
    }
}

impl<B: Backend> RBTree<u32> for TreeAllocator<B> {
    fn root(&self) -> u32 {
        self.tree_root
    }

    fn root_mut(&mut self) -> &mut u32 {
        &mut self.tree_root
    }

    fn get(&self, id: u32) -> &RBTreeNode<u32> {
        &self.blocks[id as usize].tree_node
    }

    fn get_mut(&mut self, id: u32) -> &mut RBTreeNode<u32> {
        &mut self.blocks[id as usize].tree_node
    }

    fn order(&self, a: u32, b: u32) -> bool {
        self.blocks[a as usize].size >= self.blocks[b as usize].size
    }
}

impl<B: Backend> Allocator<B> for TreeAllocator<B> {
    fn alloc(
        &mut self,
        device: &B::Device,
        memory_info: &MemoryInfo,
        memory_type: MemoryTypeId,
        properties: MemoryProperties,
        req: &MemoryRequest,
    ) -> Result<MemoryBlock<B>, AllocationError> {
        let alignment = if properties.contains(MemoryProperties::CPU_VISIBLE)
            && !properties.contains(MemoryProperties::COHERENT)
        {
            req.alignment.min(memory_info.non_coherent_atom_size)
        } else {
            req.alignment
        };

        let smallest = self.find_min(|id| {
            let block = &self.blocks[id as usize];
            req.size <= block.size
        });

        let mut best_fit = smallest;
        let mut retries = 0;
        let mut offset = 0;
        let mut alignment_offset = 0;

        while best_fit != u32::MAX {
            let block = &self.blocks[best_fit as usize];
            offset = align_up(block.offset, alignment);
            alignment_offset = offset - block.offset;

            if block.prev_block != u32::MAX {
                let prev = &self.blocks[block.prev_block as usize];
                if let (Some(a), Some(b)) = (block.contents, prev.contents) {
                    if a.conflict(b) {
                        alignment_offset =
                            alignment_offset.max(memory_info.buffer_image_granularity);
                    }
                }
            }

            if alignment_offset + req.size <= block.size {
                break;
            }

            retries += 1;

            if retries < 10 {
                best_fit = self.successor(best_fit);
            } else {
                best_fit = self.find_min(|id| {
                    let block = &self.blocks[id as usize];
                    req.size + alignment - 1 <= block.size
                });
            }
        }

        // dbg!(amt);

        let (memory, offset, id) = if best_fit == u32::MAX {
            let new_chunk_size = align_up(
                req.size.max(self.config.chunk_min_size),
                self.config.chunk_multiple,
            );

            let memory = unsafe {
                device
                    .allocate_memory(
                        gfx_hal::MemoryTypeId(memory_type.0 as usize),
                        new_chunk_size as u64,
                    )
                    .map(Box::new)
                    .map(Box::into_raw)?
            };

            let used_block = TreeAllocatorBlock {
                memory,
                offset: 0,
                size: req.size,
                tree_node: Default::default(),
                next_block: u32::MAX,
                prev_block: u32::MAX,
                is_free: false,
                contents: Some(req.contents),
            };

            let used_block_id = self.blocks.insert(used_block) as u32;

            if new_chunk_size > req.size {
                let free_block = TreeAllocatorBlock {
                    memory,
                    offset: req.size,
                    size: new_chunk_size - req.size,
                    tree_node: Default::default(),
                    next_block: u32::MAX,
                    prev_block: used_block_id,
                    is_free: true,
                    contents: None,
                };

                let free_block_id = self.blocks.insert(free_block) as u32;
                self.insert(free_block_id);
                self.blocks[used_block_id as usize].next_block = free_block_id;
            }

            (memory as *const _, 0, used_block_id)
        } else {
            self.remove(best_fit);
            let free_block = self.blocks.remove(best_fit as usize);
            let memory = free_block.memory;

            let prev_prev = free_block.prev_block;
            let next_next = free_block.next_block;

            let mut prev_block = free_block.prev_block;
            let mut next_block = free_block.next_block;

            if alignment_offset > 0 {
                let left_free = TreeAllocatorBlock {
                    memory,
                    offset: free_block.offset,
                    size: alignment_offset,
                    tree_node: Default::default(),
                    next_block: u32::MAX,
                    prev_block: prev_prev,
                    is_free: true,
                    contents: None,
                };

                let left_free_id = self.blocks.insert(left_free) as u32;
                self.insert(left_free_id);

                if prev_prev != u32::MAX {
                    self.blocks[prev_prev as usize].next_block = left_free_id;
                }

                prev_block = left_free_id;
            }

            let free_right = free_block.size - alignment_offset - req.size;
            if free_right > 0 {
                let right_free = TreeAllocatorBlock {
                    memory,
                    offset: offset + req.size,
                    size: free_right,
                    tree_node: Default::default(),
                    next_block: next_next,
                    prev_block: u32::MAX,
                    is_free: true,
                    contents: None,
                };

                let right_free_id = self.blocks.insert(right_free) as u32;
                self.insert(right_free_id);

                if next_next != u32::MAX {
                    self.blocks[next_next as usize].prev_block = right_free_id;
                }

                next_block = right_free_id;
            }

            let used_block = TreeAllocatorBlock {
                memory,
                offset,
                size: req.size,
                tree_node: Default::default(),
                next_block,
                prev_block,
                is_free: false,
                contents: Some(req.contents),
            };

            let used_block_id = self.blocks.insert(used_block) as u32;

            if prev_block != u32::MAX {
                self.blocks[prev_block as usize].next_block = used_block_id;
            }

            if next_block != u32::MAX {
                self.blocks[next_block as usize].prev_block = used_block_id;
            }

            (memory, offset, used_block_id)
        };

        Ok(MemoryBlock {
            memory,
            memory_type,
            allocator: 0,
            metadata: id,
            offset,
            size: req.size,
        })
    }

    unsafe fn free(&mut self, device: &B::Device, mem_block: MemoryBlock<B>) -> FreeResult {
        let prev_block = self.blocks[mem_block.metadata as usize].prev_block;
        let next_block = self.blocks[mem_block.metadata as usize].next_block;

        let has_prev = prev_block != u32::MAX && self.blocks[prev_block as usize].is_free;
        let has_next = next_block != u32::MAX && self.blocks[next_block as usize].is_free;

        let prev_first = prev_block == u32::MAX
            || (has_prev && self.blocks[prev_block as usize].prev_block == u32::MAX);

        let next_last = next_block == u32::MAX
            || (has_next && self.blocks[next_block as usize].next_block == u32::MAX);

        if prev_first && next_last {
            if has_prev {
                self.remove(prev_block);
                self.blocks.remove(prev_block as usize);
            }

            if has_next {
                self.remove(next_block);
                self.blocks.remove(next_block as usize);
            }

            let block = self.blocks.remove(mem_block.metadata as usize);
            let memory = Box::from_raw(block.memory as *mut _);
            device.free_memory(*memory);
            return FreeResult::ChunkFreed;
        }

        if has_prev && has_next {
            self.remove(prev_block);
            self.remove(next_block);
            self.blocks[prev_block as usize].size +=
                self.blocks[next_block as usize].size + mem_block.size;
            self.insert(prev_block);

            let next = self.blocks[next_block as usize].next_block;
            self.blocks[prev_block as usize].next_block = next;
            if next != u32::MAX {
                self.blocks[next as usize].prev_block = prev_block;
            }

            self.blocks.remove(next_block as usize);
            self.blocks.remove(mem_block.metadata as usize);
        } else if has_prev {
            self.remove(prev_block);
            self.blocks[prev_block as usize].size += mem_block.size;
            self.insert(prev_block);

            self.blocks[prev_block as usize].next_block = next_block;
            if next_block != u32::MAX {
                self.blocks[next_block as usize].prev_block = prev_block;
            }

            self.blocks.remove(mem_block.metadata as usize);
        } else if has_next {
            self.remove(next_block);
            self.blocks[next_block as usize].size += mem_block.size;
            self.blocks[next_block as usize].offset = mem_block.offset;
            self.insert(next_block);

            self.blocks[next_block as usize].prev_block = prev_block;
            if prev_block != u32::MAX {
                self.blocks[prev_block as usize].next_block = next_block;
            }

            self.blocks.remove(mem_block.metadata as usize);
        } else {
            self.blocks[mem_block.metadata as usize].is_free = true;
            self.insert(mem_block.metadata);
        }

        FreeResult::ChunkInUse
    }

    unsafe fn destroy(self, device: &B::Device) {
        let mut allocs = AHashSet::default();
        for (_, block) in self.blocks {
            allocs.insert(block.memory);
        }

        for alloc in allocs {
            let boxed = Box::from_raw(alloc as *mut _);
            device.free_memory(*boxed);
        }
    }

    fn report(&self, builder: &mut ReportBuilder) {
        let start_blocks = self.blocks.iter().filter(|(_, c)| c.prev_block == u32::MAX);
        for (_, start_block) in start_blocks {
            let label = format!("{:?}", start_block.memory);
            let mut blocks = vec![(start_block.size, start_block.is_free)];
            let mut node = start_block.next_block;
            while node != u32::MAX {
                let next = &self.blocks[node as usize];
                blocks.push((next.size, next.is_free));
                node = next.next_block;
            }
            builder.chunk(&label, &blocks);
        }
    }
}
