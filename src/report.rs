use std::fmt::Write;

use humansize::FileSize;

pub struct ReportBuilder {
    html: String,
    num_sections: usize,
}

impl ReportBuilder {
    pub fn new() -> ReportBuilder {
        let html = r#"
            <!doctype html>
            <html>
            <head>
                <title>Memory Report</title>
                <style>
                    body {
                        max-width: 1000px;
                        margin: 20px auto;
                        padding: 20px;
                    }

                    .chunk {
                        display: flex; 
                        flex-wrap: wrap;
                        padding: 8px 0;
                    }

                    .chunk-blocks {
                        display: flex; 
                        width: 100%;
                        height: 20px;
                        border: 1px solid #333;
                    }

                    .chunk-block {
                        height: 20px;
                    }

                    .block-used {
                        background: #555;
                    }

                    .block-used:hover {
                        background: #f55;
                    }

                    .chunk-label {
                        flex: 1 0 auto;
                    }
                </style>
            </head>
            <body>
        "#;
        ReportBuilder {
            html: String::from(html),
            num_sections: 0,
        }
    }

    pub fn section(&mut self, name: &str) {
        if self.num_sections > 0 {
            self.html += "<hr>";
        }

        let _ = write!(&mut self.html, "<h3>{}</h3>", name);
        self.num_sections += 1;
    }

    pub fn label(&mut self, text: &str) {
        let _ = write!(&mut self.html, "<p>{}</p>", text);
    }

    pub fn chunk(&mut self, label: &str, blocks: &[(u32, bool)]) {
        let total_size = blocks.iter().map(|(s, _)| s).sum::<u32>();
        let it = blocks.iter();
        let free_size = it.filter(|(_, f)| *f).map(|(s, _)| s).sum::<u32>();
        let it = blocks.iter();
        let used_size = it.filter(|(_, f)| !f).map(|(s, _)| s).sum::<u32>();

        self.html += "<div class=\"chunk\">";

        let _ = write!(&mut self.html, "<div class=\"chunk-label\">{}</div>", label);
        let _ = write!(
            &mut self.html,
            "<div class=\"chunk-size\">{} ({:.1}%) + {} = {}</div>",
            format_size(used_size),
            used_size as f32 / total_size as f32 * 100.0,
            format_size(free_size),
            format_size(total_size)
        );

        self.html += "<div class=\"chunk-blocks\">";

        for &(size, free) in blocks {
            let width = 100.0 * (size as f32 / total_size as f32);
            let class = if free {
                "chunk-block block-free"
            } else {
                "chunk-block block-used"
            };

            let _ = write!(
                &mut self.html,
                "<div class=\"{}\" style=\"flex:{} 0 auto\" title=\"{}\"></div>",
                class,
                width,
                format_size(size)
            );
        }

        self.html += "</div>";
        self.html += "</div>";
    }

    pub fn build(mut self) -> String {
        self.html += "</body>";
        self.html += "</html>";
        self.html
    }
}

fn format_size(v: u32) -> String {
    v.file_size(humansize::file_size_opts::BINARY).unwrap()
}
