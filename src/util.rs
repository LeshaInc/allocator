pub mod rbtree;

pub fn align_up(addr: u32, alignment: u32) -> u32 {
    (addr + alignment - 1) & !(alignment - 1)
}
