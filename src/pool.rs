use std::collections::hash_map::Entry;
use std::ops::{Bound, Range, RangeBounds};
use std::sync::Arc;

use ahash::AHashMap;
use gfx_hal::device::{Device, OutOfMemory};
use gfx_hal::memory::Segment;
use gfx_hal::Backend;

use super::{
    AllocationError, Allocator, DynamicAllocator, FreeResult, LinearAllocator, MemoryBlock,
    MemoryInfo, MemoryProperties, MemoryRequest, MemoryTypes, ReportBuilder,
};

/// Memory pool.
#[derive(Debug)]
pub struct MemoryPool<B: Backend, A = DynamicAllocator<B>> {
    allocators: Vec<A>,
    mappings: AHashMap<*const B::Memory, *mut u8>,
    memory_types: Arc<MemoryTypes>,
    memory_info: Arc<MemoryInfo>,
}

impl<B: Backend, A: Allocator<B>> MemoryPool<B, A> {
    /// Create a new pool with default allocator.
    pub fn new(types: Arc<MemoryTypes>, info: Arc<MemoryInfo>) -> MemoryPool<B, A>
    where
        A: Default,
    {
        Self::new_with(types, info, || A::default())
    }

    /// Create a new pool with custom allocator.
    pub fn new_with(
        types: Arc<MemoryTypes>,
        info: Arc<MemoryInfo>,
        mut builder: impl FnMut() -> A,
    ) -> MemoryPool<B, A> {
        MemoryPool {
            allocators: (0..types.num_types()).map(|_| builder()).collect(),
            mappings: AHashMap::default(),
            memory_types: types,
            memory_info: info,
        }
    }

    pub fn report(&mut self, builder: &mut ReportBuilder) {
        for allocator in &self.allocators {
            allocator.report(builder);
        }
    }

    /// Allocate a memory block. Returns `None` when the request cannot be fulfilled.
    pub fn alloc(
        &mut self,
        device: &B::Device,
        req: &MemoryRequest,
    ) -> Result<MemoryBlock<B>, AllocationError> {
        for (memory_type, properties) in self.memory_types.get(req.usage) {
            if req.type_mask & (1 << memory_type.0) != 0 {
                return self.allocators[memory_type.0 as usize].alloc(
                    device,
                    &self.memory_info,
                    memory_type,
                    properties,
                    req,
                );
            }
        }

        Err(AllocationError::NoSuitableTypes)
    }

    pub unsafe fn map_read<'a, 's: 'a>(
        &'s mut self,
        device: &'a B::Device,
        block: &'a MemoryBlock<B>,
    ) -> Result<ReadMapping<'a, B>, MapError> {
        let properties = self.memory_types.properties(block.memory_type);
        if !properties.contains(MemoryProperties::CPU_VISIBLE) {
            return Err(MapError::NotVisible);
        }

        let entry = self.mappings.entry(block.memory);
        let ptr = match entry {
            Entry::Vacant(slot) => {
                let ptr = device.map_memory(&mut *(block.memory as *mut _), Segment::ALL)?;
                *slot.insert(ptr)
            }
            Entry::Occupied(mut slot) => *slot.get_mut(),
        };

        Ok(ReadMapping {
            ptr,
            is_coherent: properties.contains(MemoryProperties::COHERENT),
            block,
            device,
        })
    }

    pub unsafe fn map_write<'a, 's: 'a>(
        &'s mut self,
        device: &'a B::Device,
        block: &'a mut MemoryBlock<B>,
    ) -> Result<WriteMapping<'a, B>, MapError> {
        let mapping = self.map_read(device, block)?;
        Ok(WriteMapping {
            ptr: mapping.ptr as *mut _,
            is_coherent: mapping.is_coherent,
            block,
            device,
        })
    }

    /// Free a block. Block must be allocated from this pool and not in use.
    pub unsafe fn free(&mut self, device: &B::Device, block: MemoryBlock<B>) {
        let ptr = block.memory;
        let res = self.allocators[block.memory_type.0 as usize].free(device, block);
        if res == FreeResult::ChunkFreed {
            self.mappings.remove(&ptr);
        }
    }

    /// Destroy the pool. All blocks must be freed.
    pub unsafe fn destroy(mut self, device: &B::Device) {
        for allocator in self.allocators.drain(..) {
            allocator.destroy(device);
        }
    }
}

impl<B: Backend> MemoryPool<B, LinearAllocator<B>> {
    /// Free all blocks in the pool. Blocks must be not in use.
    pub unsafe fn free_all(&mut self, device: &B::Device) {
        for allocator in &mut self.allocators {
            allocator.free_all(device);
        }
    }
}

#[derive(thiserror::Error, Debug)]
pub enum MapError {
    #[error(transparent)]
    GfxError(#[from] gfx_hal::device::MapError),
    #[error("Trying to map memory which is not cpu visible")]
    NotVisible,
}

pub struct ReadMapping<'a, B: Backend> {
    ptr: *const u8,
    is_coherent: bool,
    block: &'a MemoryBlock<B>,
    device: &'a B::Device,
}

impl<'a, B: Backend> ReadMapping<'a, B> {
    pub unsafe fn invalidate(&mut self, range: impl RangeBounds<u32>) -> Result<(), OutOfMemory> {
        if self.is_coherent {
            return Ok(());
        }

        let range = convert_range(self.block.size, range);

        self.device
            .invalidate_mapped_memory_ranges(std::iter::once((
                &*self.block.memory,
                Segment {
                    offset: (self.block.offset + range.start) as u64,
                    size: Some((range.end - range.start) as u64),
                },
            )))
    }

    pub unsafe fn read(&self, range: impl RangeBounds<u32>) -> &[u8] {
        let range = convert_range(self.block.size, range);
        let len = (range.end - range.start) as usize;
        let ptr = self.ptr.offset(range.start as isize);
        std::slice::from_raw_parts(ptr, len)
    }
}

pub struct WriteMapping<'a, B: Backend> {
    ptr: *mut u8,
    is_coherent: bool,
    block: &'a mut MemoryBlock<B>,
    device: &'a B::Device,
}

impl<'a, B: Backend> WriteMapping<'a, B> {
    pub unsafe fn invalidate(&mut self, range: impl RangeBounds<u32>) -> Result<(), OutOfMemory> {
        if self.is_coherent {
            return Ok(());
        }

        let range = convert_range(self.block.size, range);

        self.device
            .invalidate_mapped_memory_ranges(std::iter::once((
                &*self.block.memory,
                Segment {
                    offset: (self.block.offset + range.start) as u64,
                    size: Some((range.end - range.start) as u64),
                },
            )))
    }

    pub unsafe fn read(&self, range: impl RangeBounds<u32>) -> &[u8] {
        let range = convert_range(self.block.size, range);
        let len = (range.end - range.start) as usize;
        let ptr = self.ptr.offset(range.start as isize);
        std::slice::from_raw_parts(ptr, len)
    }

    pub unsafe fn write(&mut self, range: impl RangeBounds<u32>) -> &mut [u8] {
        let range = convert_range(self.block.size, range);
        let len = (range.end - range.start) as usize;
        let ptr = self.ptr.offset(range.start as isize);
        std::slice::from_raw_parts_mut(ptr, len)
    }

    pub unsafe fn flush(&mut self, range: impl RangeBounds<u32>) -> Result<(), OutOfMemory> {
        if self.is_coherent {
            return Ok(());
        }

        let range = convert_range(self.block.size, range);

        self.device.flush_mapped_memory_ranges(std::iter::once((
            &*self.block.memory,
            Segment {
                offset: (self.block.offset + range.start) as u64,
                size: Some((range.end - range.start) as u64),
            },
        )))
    }
}

fn convert_range(size: u32, range: impl RangeBounds<u32>) -> Range<u32> {
    let start = match range.start_bound() {
        Bound::Included(&v) => v,
        Bound::Excluded(&v) => v + 1,
        Bound::Unbounded => 0,
    };

    let end = match range.end_bound() {
        Bound::Included(&v) => v + 1,
        Bound::Excluded(&v) => v,
        Bound::Unbounded => size,
    };

    assert!(start <= end && end <= size);

    start..end
}
