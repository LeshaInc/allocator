mod dedicated;
mod dynamic;
mod linear;
mod tree;

use gfx_hal::Backend;

pub use self::dedicated::DedicatedAllocator;
pub use self::dynamic::{DynamicAllocator, DynamicAllocatorConfig};
pub use self::linear::{LinearAllocator, LinearAllocatorConfig};
pub use self::tree::{TreeAllocator, TreeAllocatorConfig};
use super::{MemoryInfo, MemoryProperties, MemoryRequest, MemoryTypeId};
use crate::ReportBuilder;

/// Memory block (suballocation).
#[derive(Debug)]
pub struct MemoryBlock<B: Backend> {
    pub(crate) memory: *const B::Memory,
    pub(crate) memory_type: MemoryTypeId,
    pub(crate) allocator: u8,
    pub(crate) metadata: u32,
    pub(crate) offset: u32,
    pub(crate) size: u32,
}

// I know what I am doing.
unsafe impl<B: Backend> Send for MemoryBlock<B> {}
unsafe impl<B: Backend> Sync for MemoryBlock<B> {}

/// Allocator from a single memory type.
pub trait Allocator<B: Backend> {
    /// Allocate a block. `None` when out of memory.
    fn alloc(
        &mut self,
        device: &B::Device,
        memory_info: &MemoryInfo,
        memory_type: MemoryTypeId,
        properties: MemoryProperties,
        req: &MemoryRequest,
    ) -> Result<MemoryBlock<B>, AllocationError>;

    /// Free a block. Block must come from this allocator and be not in use.
    unsafe fn free(&mut self, device: &B::Device, block: MemoryBlock<B>) -> FreeResult;

    /// Destroy the allocator. All blocks must be freed.
    unsafe fn destroy(self, device: &B::Device);

    fn report(&self, builder: &mut ReportBuilder) {
        let _ = builder;
    }
}

#[derive(thiserror::Error, Debug)]
pub enum AllocationError {
    #[error("Could not allocate memory chunk")]
    ChunkAllocation(#[from] gfx_hal::device::AllocationError),
    #[error("Could not find suitable memory type")]
    NoSuitableTypes,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum FreeResult {
    ChunkFreed,
    ChunkInUse,
}
